package com.example.reliability;

import com.example.util.RabbitMqConnection;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ReliabilityProduct {
    // 可靠性投递
    // 1、消息持久化

    public static void main(String[] args) throws IOException {


        Connection connection = RabbitMqConnection.getConnection();
        Channel channel = connection.createChannel();
        // 声明队列
        String directQueue = "direct_durable_queue";
        channel.queueDeclare(directQueue, true, false, false, null);
        // 声明交换机
        String exchange = "direct_durable_exchange";
        channel.exchangeDeclare(exchange, BuiltinExchangeType.DIRECT);
        // 绑定交换机和队列
        channel.queueBind(directQueue, exchange, "order.add");
        String message1 = "hello Direct Durable Add Queue!";

        // 设置消息
        AMQP.BasicProperties basic = MessageProperties.PERSISTENT_TEXT_PLAIN;
        for (int i = 0; i < 10; i++) {
            channel.basicPublish(exchange, "order.add", basic, (message1 + i).getBytes(StandardCharsets.UTF_8));
        }
        RabbitMqConnection.closeConnection(connection, channel);

    }
}
