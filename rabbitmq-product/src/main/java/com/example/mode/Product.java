package com.example.mode;

import com.example.util.RabbitMqConnection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Product {
    // 简单队列,发送消息
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqConnection.getConnection();
        Channel channel = null;
        channel = connection.createChannel();
        // 声明简单队列
        channel.queueDeclare("simple_queue", true, false, false, null);
        String message = "hello Simple Queue";
        // 发送消息到队列
        channel.basicPublish("", "simple_queue", null, message.getBytes(StandardCharsets.UTF_8));
        RabbitMqConnection.closeConnection(connection, channel);
    }
}
