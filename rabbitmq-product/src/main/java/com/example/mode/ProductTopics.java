package com.example.mode;

import com.example.util.RabbitMqConnection;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ProductTopics {

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqConnection.getConnection();
        Channel channel = connection.createChannel();
        // 声明队列
        String directQueue = "topics_queue";
        channel.queueDeclare(directQueue, true, false, false, null);
        // 声明交换机
        String exchange = "topics_exchange";
        channel.exchangeDeclare(exchange, BuiltinExchangeType.TOPIC);
        // 绑定交换机和队列
        channel.queueBind(directQueue, exchange, "order.*");
        String message1 = "hello Direct Add Queue!";
        String message2 = "hello Direct Update  Queue!";
        for (int i = 0; i < 10; i++) {
            channel.basicPublish(exchange, "order.add", null, (message1 + i).getBytes(StandardCharsets.UTF_8));
            channel.basicPublish(exchange, "order.update", null, (message2 + i).getBytes(StandardCharsets.UTF_8));
        }
        RabbitMqConnection.closeConnection(connection, channel);
    }
}
