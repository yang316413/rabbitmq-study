package com.example.mode;

import com.example.util.RabbitMqConnection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ProductWorkQueue {


    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqConnection.getConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("work_queue", true, false, false, null);
        String message = "hello Work Queue!";
        for (int i = 0; i < 10; i++) {
            channel.basicPublish("", "work_queue", null, (message + i).getBytes(StandardCharsets.UTF_8));
        }
        RabbitMqConnection.closeConnection(connection, channel);
    }
}
