package com.example.mode;

import com.example.util.RabbitMqConnection;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ProductFanout {

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitMqConnection.getConnection();
        Channel channel = connection.createChannel();

        // 声明队列
        String queue1 = "fanout_queue1";
        String queue2 = "fanout_queue2";

        channel.queueDeclare(queue1, true, false, false, null);
        channel.queueDeclare(queue2, true, false, false, null);
        // 声明交换机
        String exchange = "fanout_exchange";
        channel.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT);

        channel.queueBind(queue1, exchange, "");
        channel.queueBind(queue2, exchange, "");


        String message = "hello Fanout Queue!";
        for (int i = 0; i < 10; i++) {
            channel.basicPublish(exchange, "", null, (message + i).getBytes(StandardCharsets.UTF_8));
        }
        RabbitMqConnection.closeConnection(connection, channel);
    }
}
